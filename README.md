# Colortext
This module serves as a sort of wrapper around nim's terminal module to ease
basic text colorization for things such as logging modules.

## Usage
See `include/colortext.nim` for documentation or execute `nake docs` to compile
HTML documentation.

You can also see the `examples` directory for usage samples, and a general
usage guide exists in `docs/`.

