# Usage Overview

## Concept
The whole idea behind this system is that generally when one wants to output
colored text, they want to do so multiple times per a message, with each
line being able to be broken down into multiple `elements` with various
attributes for each.

This paradigm is represented by `LineElement`'s and `Line`'s. A `Line` is
an alias for `seq[LineElement]`, and `LineElement` is an object containing the
various attributes and element data.


## Usage
Generally it is intended to construct a new `Line` with the various
`LineElement`'s being created in-place within the call. After that one need
merely call the desired `termWrite*` function.

