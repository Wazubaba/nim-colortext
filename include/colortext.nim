import terminal
export ForeGroundColor, BackgroundColor, Style

type
  Line* = seq[LineElement] ## Contains a line of elements to be printed out with specified attributes per an element

  LineElement* = ref LineElementObj # Contains an individual element of a line of text with attribute data
  LineElementObj = object
    style: set[Style]
    fg: ForeGroundColor
    bg: BackgroundColor
    element: string
    endingSpace: bool

when not defined(Windows):
  const FG_DEFAULT = fgDefault
  const BG_DEFAULT = bgDefault
else:
  const FG_DEFAULT = fgWhite
  const BG_DEFAULT = bgBlack

func newLineElement*(element: string, endingSpace = true, style:set[Style] = {}, fg = FG_DEFAULT, bg = BG_DEFAULT): LineElement =
  ## Construct a new element with the given attributes
  return LineElement(
    style: style,
    endingSpace: endingSpace,
    fg: fg,
    bg: bg,
    element: element)

func `@`*(data: string): LineElement =
  ## Helper to construct a new element with default attributes and ending with a space
  result = newLineElement(data)

func `@!`*(data: string): LineElement =
  ## Helper to construct a new element with default attributes and ending with no space
  result = newLineElement(data, false)

func newLine*(elements: varargs[LineElement]): Line =
  ## Construct a new line container with the given elements
  result = newSeq[LineElement]()
 # result.data = 
  for item in elements:
    result.add(item)

func concat*(lines: varargs[Line]): Line =
  ## Concatenate all given Lines into one, fifo-style
  result = concat(lines)

proc termWrite*(file = stdout, useColors=true, line: Line) =
  ## Write a line container to the given file
  for item in line:
    if useColors:
      file.setStyle(item.style)
      file.setForeGroundColor(item.fg)
      file.setBackGroundColor(item.bg)
    file.write(item.element)

    if useColors:
      file.resetAttributes()
    if item.endingSpace: file.write(" ")

proc termWriteLine*(file = stdout, useColors = true, lines: varargs[Line]) =
  for line in lines:
    file.termWrite(useColors, line)
    file.writeLine("")

proc termWriteLine*(lines: varargs[Line]) =
  termWriteLine(stdout, true, lines)

proc termWrite*(line: Line) =
  termWrite(stdout, true, line)

